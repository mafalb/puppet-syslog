require 'rspec-puppet'

fixture_path = File.expand_path(File.join(__FILE__, '..', 'fixtures'))
fixture_module_path = File.join(fixture_path, 'modules')

Puppet.parse_config
puppet_module_path = Puppet[:modulepath]

RSpec.configure do |c|
  c.module_path = [fixture_path, puppet_module_path].join(":")
  c.module_path = File.join(fixture_path, 'modules')
  c.manifest_dir = File.join(fixture_path, 'manifests')
end

Puppet.parse_config
puppet_module_path = Puppet[:modulepath]

RSpec.configure do |c|
  c.module_path = fixture_module_path
  c.manifest_dir = File.join(fixture_path, 'manifests')
end
