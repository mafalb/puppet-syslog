# puppet syslog module

This handles the syslog daemon.

The web page for contributing is at [https://bitbucket.org/mafalb/puppet-syslog][2]  
The puppet forge link is [http://forge.puppetlabs.com/mafalb/syslog][3]

## Requirements

It uses hiera, so hiera must be installed on your puppet master or on any machines you want to run the testsuite on.

## Usage
    
    # activate the syslog service
    include syslog

maybe you want another syslogger do the work

    # deactivate the syslog service
    include syslog::off

or

    # get rid of it
    include syslog::rm

[2]: https://bitbucket.org/mafalb/puppet-syslog
[3]: http://forge.puppetlabs.com/mafalb/syslog